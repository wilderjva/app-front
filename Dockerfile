### STAGE 1: Build ###
FROM node:15.14.0-alpine3.10 AS build
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
RUN npm run build -- --prod --output-path=dist/front

### STAGE 2: Run ###
FROM nginx:1.19.10-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /app/dist/front /usr/share/nginx/html/
